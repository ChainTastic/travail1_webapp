class Produit:
    def __init__(self, id_produit, nom, description, prix, coutant, taxable_federal, taxable_provincial, categorie, magasin):
        self.id = id_produit
        self.nom = nom
        self.description = description
        self.prix = prix
        self.coutant = coutant
        self.taxableFederal = taxable_federal
        self.taxableProvincial = taxable_provincial
        self.categorie = categorie
        self.magasin = magasin or None
