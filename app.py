from flask import Flask, render_template, abort, redirect, request, url_for, session
from bd import obtenir_connexion
from produit import Produit
from magasin import Magasin
from compte import Compte

app = Flask(__name__)


app.secret_key = b'=c\xe5\xcc\xd2\xc1\xe5\x93\xbdo\x93\xd9\xbf\xdd\xfc%'


@app.route('/', methods=['GET'])
def index():
    if "utilisateur" in session:
        connecter = True
        uid = session['utilisateur']
        connexion = obtenir_connexion()
        curseur = connexion.cursor()
        curseur.execute("SELECT isAdmin FROM comptes WHERE id = %s", (uid,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            return redirect(url_for('lister_magasins', connecter=connecter))
        return redirect(url_for('admin', connecter=connecter))
    return redirect(url_for('authentification'))


@app.route('/authentification/connexion')
def authentification():
    return render_template("compte/authentification.html")


@app.route('/authentification/connexion', methods=['POST'])
def verifier_authentification():
    username = request.form.get('username')
    mdp = request.form.get('password')
    if not username:
        message = "Nom d'utilisateur manquant"
        return render_template("compte/authentification.html", message=message)
    elif not mdp:
        message = "Mot de passe manquant"
        return render_template("compte/authentification.html", message=message)
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    try:
        print(username)
        print(mdp)
        curseur.execute("SELECT id, isAdmin FROM comptes WHERE username =%s AND password =%s", (username, mdp))
        resultat = curseur.fetchone()
        if resultat is None:
            messages = "Information incorrecte"
            return render_template("compte/authentification.html", messages=[messages])
        uid, est_admin = resultat
        session["utilisateur"] = uid
        if est_admin == 1:
            return redirect(url_for("admin"))
        return redirect(url_for("lister_magasins"))
    finally:
        connexion.close()


@app.route('/admin')
def admin():
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    try:
        if 'utilisateur' not in session:
            abort(401)
        uid = session['utilisateur']
        connecter = True
        curseur.execute("SELECT isAdmin FROM comptes WHERE id = %s", (uid,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            abort(403)
        return render_template('administration/admin.html', connecter=connecter)
    finally:
        connexion.close()


@app.route('/admin/comptes')
def gerer_comptes():
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    try:
        if 'utilisateur' not in session:
            abort(401)
        uid = session['utilisateur']
        connecter = True
        curseur.execute("SELECT isAdmin FROM comptes WHERE id = %s", (uid,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            abort(403)
        curseur.execute("SELECT * FROM comptes")
        resultat = curseur.fetchall()
        liste_comptes = []
        for id_compte, nom_utilisateur, mdp, est_admin in resultat:
            compte = Compte(id_compte, nom_utilisateur, mdp, est_admin)
            print(id_compte,compte.uid)
            print(nom_utilisateur,compte.nom)
            liste_comptes.append(compte)
        return render_template('administration/comptes.html', comptes=liste_comptes, connecter=connecter)
    finally:
        connexion.close()


@app.route('/admin/comptes/creer')
def creation_compte():
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    try:
        if 'utilisateur' not in session:
            abort(401)
        uid = session['utilisateur']
        connecter = True
        curseur.execute("SELECT isAdmin FROM comptes WHERE id = %s", (uid,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            abort(403)
        return render_template('compte/compte.html', connecter=connecter)
    finally:
        connexion.close()


@app.route('/admin/comptes/creer', methods=['POST'])
def creer_compte():
    if 'utilisateur' not in session:
        abort(401)
    connecter = True
    connexion = obtenir_connexion()
    messages = []
    nom_utilisateur = request.form['username']
    mdp = request.form['password']
    est_admin = 'admin' in request.form or False
    if nom_utilisateur == "":
        messages.append("Nom d'utilisateur obligatoire")
    elif mdp == "":
        messages.append("Mot de passe obligatoire")
    if len(messages) > 0:
        return render_template("compte/compte.html", connecter=connecter, messages=messages)
    try:
        curseur = connexion.cursor()
        curseur.execute("INSERT INTO comptes (username, password, isAdmin) VALUES (%s, %s, %s)",
                        (nom_utilisateur, mdp, est_admin))
        connexion.commit()
        return redirect(url_for('gerer_comptes', connecter=connecter))
    finally:
        connexion.close()


@app.route("/admin/comptes/<int:uid>/supprimer")
def supprimer_compte(uid):
    connexion = obtenir_connexion()
    try:
        curseur = connexion.cursor()
        if 'utilisateur' not in session:
            abort(401)
        user = session['utilisateur']
        connecter = True
        curseur.execute("SELECT isAdmin FROM comptes WHERE id = %s", (user,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            abort(403)
        curseur.execute("DELETE FROM comptes WHERE id = %s", (uid,))
        connexion.commit()
        return redirect(url_for('gerer_comptes', connecter=connecter))

    finally:
        connexion.close()


@app.route('/magasins')
def lister_magasins():
    connexion = obtenir_connexion()
    try:
        if 'utilisateur' not in session:
            abort(401)
        connecter = True
        curseur = connexion.cursor()
        curseur.execute("SELECT * FROM magasin")
        resultats = curseur.fetchall()
        liste_magasins = []
        for resultat in resultats:
            magasin = Magasin(resultat[0], resultat[1])
            liste_magasins.append(magasin)
        return render_template('magasin/liste_magasins.html', connecter=connecter, listeMagasins=liste_magasins)
    finally:
        connexion.close()


@app.route('/magasins/<int:id_magasin>', methods=['GET'])
def lister_produits(id_magasin):
    connexion = obtenir_connexion()
    try:
        if 'utilisateur' not in session:
            abort(401)
        connecter = True
        curseur = connexion.cursor()
        if id_magasin == 0:
            curseur.execute("SELECT produitId, produit.nom, description, prix, coutant, taxableFederal, taxableProvincial, "
                            "type_produit.nom FROM produit,type_produit WHERE idMagasin IS NULL AND typeProduitId = type_produit.id")
        else:
            curseur.execute(
                "SELECT produitId, produit.nom, description, prix, coutant, taxableFederal, taxableProvincial, "
                "type_produit.nom FROM produit,type_produit WHERE idMagasin =%s AND typeProduitId = type_produit.id",
                (id_magasin,))
        liste_produits = []
        for idProduit, nom, desc, prix, coutant, taxableFederal, taxableProvincial, categorieProduit in curseur:
            produit = Produit(idProduit, nom, desc, prix, coutant, taxableFederal, taxableProvincial, categorieProduit,
                              id_magasin)
            liste_produits.append(produit)
        curseur.execute("SELECT nom from magasin where magasinId =%s", (id_magasin,))
        if id_magasin == 0:
            nom_magasin = "Tous"
        else:
            nom_magasin, = curseur.fetchone()
        if nom_magasin is None:
            abort(404)
        magasin = Magasin(id_magasin, nom_magasin)
        return render_template('produit/liste_produits.html', connecter=connecter, listeProduits=liste_produits,
                               nomMagasin=magasin.nom)
    finally:
        connexion.close()


@app.route('/produits/<int:id_produit>', methods=['GET'])
def voir_produit(id_produit):
    connexion = obtenir_connexion()
    try:
        if 'utilisateur' not in session:
            abort(401)
        connecter = True
        curseur = connexion.cursor()
        curseur.execute(
            "SELECT typeProduitId, nom, description, coutant, prix, idMagasin, taxableFederal, taxableProvincial "
            "FROM produit WHERE produitId=%s", (id_produit,))
        resultat = curseur.fetchone()
        if resultat is None:
            abort(404)
        type_produit, nom_produit, desc_produit, coutant_produit, prix_produit, magasin_produit, taxe_fed, taxe_pro = resultat
        produit = Produit(id_produit, nom_produit, desc_produit, prix_produit, coutant_produit, taxe_fed, taxe_pro,
                          type_produit, magasin_produit)
        curseur.execute("SELECT magasinId, nom FROM magasin")
        liste_magasin = curseur.fetchall()
        curseur.execute("SELECT id, nom FROM type_produit")
        liste_type = curseur.fetchall()
        return render_template('produit/voir_produit.html', connecter=connecter, nom=produit.nom,
                               description=produit.description, prix=produit.prix, coutant=produit.coutant,
                               taxe_fed=produit.taxableFederal, taxe_pro=produit.taxableProvincial,
                               categorie=produit.categorie, magasin=produit.magasin, magasins=liste_magasin,
                               types=liste_type)
    finally:
        connexion.close()


@app.route('/produits/<int:id_produit>', methods=['POST'])
def modifier_produit(id_produit):
    connexion = obtenir_connexion()
    messages = []
    description = request.form['description']
    coutant = request.form['coutant']
    prix = request.form['prix']
    taxe_prov = 'taxe_prov' in request.form or None
    taxe_fed = 'taxe_fed' in request.form or None
    magasin = request.form['magasin']
    type_choisi = request.form['type']
    if description == "":
        messages.append("Nom est obligatoire")
    elif coutant == "":
        messages.append("Coutant est obligatoire")
    elif prix == "":
        messages.append("Prix est obligatoire")
    if len(messages) > 0:
        curseur = connexion.cursor()
        curseur.execute("SELECT magasinId, nom FROM magasin")
        liste_magasin = curseur.fetchall()
        curseur.execute("SELECT id, nom FROM type_produit")
        liste_type = curseur.fetchall()
        return render_template("produit/voir_produit.html", magasins=liste_magasin, types=liste_type, messages=messages)
    try:
        curseur = connexion.cursor()
        curseur.execute("UPDATE produit SET description=%s, coutant=%s, prix=%s, taxableProvincial=%s, "
                        "taxableFederal=%s, idMagasin=%s, typeProduitId=%s WHERE produitId =%s",
                        (description, coutant, prix, taxe_prov, taxe_fed, magasin, type_choisi, id_produit))
        connexion.commit()
        return redirect(url_for('lister_magasins'))
    finally:
        connexion.close()


@app.route('/produits/creer')
def creation_produit():
    connexion = obtenir_connexion()
    curseur = connexion.cursor()
    curseur.execute("SELECT magasinId, nom FROM magasin")
    liste_magasin = curseur.fetchall()
    curseur.execute("SELECT id, nom FROM type_produit")
    liste_type = curseur.fetchall()
    return render_template("produit/voir_produit.html", magasins=liste_magasin, types=liste_type)


@app.route('/produits/creer', methods=['POST'])
def creer_produit():
    if 'utilisateur' not in session:
        abort(401)
    connecter = True
    connexion = obtenir_connexion()
    messages = []
    description = request.form['description']
    coutant = request.form['coutant']
    prix = request.form['prix']
    taxe_prov = 'taxe_prov' in request.form or False
    taxe_fed = 'taxe_fed' in request.form or False
    magasin = request.form['magasin']
    type_choisi = request.form['type']
    if description == "":
        messages.append("Nom est obligatoire")
    elif coutant == "":
        messages.append("Coutant est obligatoire")
    elif prix == "":
        messages.append("Prix est obligatoire")
    if len(messages) > 0:
        curseur = connexion.cursor()
        curseur.execute("SELECT magasinId, nom FROM magasin")
        liste_magasin = curseur.fetchall()
        curseur.execute("SELECT id, nom FROM type_produit")
        liste_type = curseur.fetchall()
        return render_template("produit/voir_produit.html", connecter=connecter, magasins=liste_magasin,
                               types=liste_type, messages=messages)
    try:
        curseur = connexion.cursor()
        curseur.execute("INSERT INTO produit (nom, typeProduitId, description, stock, coutant, active, format, prix, "
                        "idMagasin, taxableFederal, taxableProvincial) VALUES ('abc' ,%s, %s, 5, %s, true, "
                        "'120 g', %s, %s, %s, %s)",
                        (type_choisi, description, coutant, prix, magasin, taxe_fed, taxe_prov))
        connexion.commit()
        return redirect(url_for('lister_magasins'))
    finally:
        connexion.close()


@app.route("/produits/<int:uid>/supprimer")
def supprimer_produit(uid):
    connexion = obtenir_connexion()
    try:
        curseur = connexion.cursor()
        if 'utilisateur' not in session:
            abort(401)
        sessionid = session['utilisateur']
        connecter = True
        curseur.execute("SELECT isAdmin FROM comptes WHERE id = %s", (sessionid,))
        est_admin, = curseur.fetchone()
        if est_admin is None or est_admin == 0:
            return redirect(url_for('lister_magasins'))
        curseur.execute("DELETE FROM produit WHERE produitId = %s", (uid,))
        connexion.commit()
        return redirect(url_for('lister_magasins', connecter=connecter))
    finally:
        connexion.close()


@app.route('/authentification/deconnexion')
def deconnecter():
    session.pop('utilisateur', None)
    return redirect(url_for("authentification"))


@app.errorhandler(404)
def erreur404(error):
    return render_template("erreurs/page404.html"), 404


@app.errorhandler(500)
def erreur500(error):
    return render_template("erreurs/page500.html"), 500


if __name__ == "__main__":
    app.run(debug=True)
